# -*- coding: utf-8 -*-
"""
Created on Sat Dec 27 14:33:07 2014

@author: Oddman
"""

import time
import collections
import cProfile
import copy

SYMBOLS = set(['{','}','=','"','#',"'"])
BRACKETS = set(['{','}'])
THINGTYPES = set([0,3,4,5,6])
PROVREPLACEHACK = False

def loadParadoxFile(savePath,fileType=None):
    scanner = Scanner(savePath)
    initialDepth = 0
    if fileType == 'ck2save':
        scanner.skipLine()
        initialDepth = 1
    tokenizer = Tokenizer(scanner)
#    print tokenizer.tokenList[:500]
    tokenDeque = collections.deque(tokenizer.tokenList)
    outDeque = tokenDeque.__copy__()
    rootObject = parse(tokenDeque,initialDepth,initialDepth)
    return outDeque,rootObject

def dumpParadoxFile(rootObject,targetPath,fileType=None):
    ## don't forget to re-put 'CK2txt'
    outstring = repr(rootObject)
    outstring = outstring.strip()
    outstring = outstring.strip('{}')## strip the brackets from the main object.
    outstring = outstring.strip()
#    print outstring
    if fileType == 'ck2save': 
        outstring = 'CK2txt\n'+outstring
    with open(targetPath,'wb') as f:
        f.write(outstring)
        
def pruneCK2Save(stuff,lateTime=10):
    ## where to remove references?
    ## character: father/mother/killer/spouse/liege = lines
    ## title history, title previous
    ## chronicle entries containing portrait =
    charKey = [k for k in stuff.keys() if k.value == 'character'][0]
    charIds = stuff[charKey].keys()
#    charlieKey = [key for key in charIds if key == '6392'][0]
    dateKey = stuff.keys()[1]
    currentDate = stuff[dateKey].value
    currentYear = int(currentDate.strip('"').split('.')[0])
    unpersons = []
    for charId in charIds:
        thisChar = stuff[charKey][charId]
        ## check whether it has a death date
        if 'death_date' in [key.value for key in thisChar.keys()]:
#            print '...'
            deathDateKey = [key for key in thisChar.keys() if key.value == 'death_date'][0]
            deathDate = stuff[charKey][charId][deathDateKey].value
            deathYear = int(deathDate.strip('"').split('.')[0])
            ## check whether it's at least lateTime years ago
            if (deathYear + lateTime) < currentYear:
                ## if so, add to lateChars
                unpersons.append(charId)
                
    unpersons = set(unpersons)
    refKeyValues = set(['father','mother','killer','spouse','liege'])
    ## then, trawl ALL characters...
    for charId in charIds:
        if charId in unpersons:
            ## remove characters that are on the lateChars list
            stuff[charKey].__delitem__(charId)
        else:       
            thisChar = stuff[charKey][charId]
        ## if not an unperson, trawl this char for:
        ## * father/mother/killer/spouse/liege-entries containing dead chars
        ## * chronicle entries with portrait == a char on the LateChars list
        removeKeys = [
            key
            for key,value 
            in thisChar.items() 
            if key.value in refKeyValues and str(value) in unpersons
        ]
        for removeKey in removeKeys:
            thisChar.__delitem__(removeKey)
        ## finally, trawl the title history and remove the entries containing dead 
        ## unpersons from there as well.
        if 'chronicle' in set([k.value for k in thisChar.keys()]):
#            print 'chronChar'
            chronKey = [k for k in thisChar.keys() if k.value == 'chronicle'][0]
            thisChar.__delitem__(chronKey)
#            ## trawl all chronicle chapters....
#            for chapterKey,chronicleChapter in thisChar[chronKey].items():
#                ## and all entries...
##                print 'chronChapter'
#                for entryKey,entryValue in chronicleChapter.items():
#                    if entryKey.value is not 'year':
#                        for propertyKey,propertyValue in entryValue.items():
#                            if str(propertyValue) in unpersons:
#    #                            print ''
#                                thisChar[chronKey][chapterKey].__delitem__(entryKey)
        elif 'chronicle_collection' in set([k.value for k in thisChar.keys()]):
            ## fuck it. just ditch the entire chronicle.
            chronColKey = [k for k in thisChar.keys() if k.value == 'chronicle_collection'][0]
            thisChar.__delitem__(chronColKey)
#            chronColKey = [k for k in thisChar.keys() if k.value == 'chronicle_collection'][0]
#            chronKey = [k for k in thisChar[chronColKey].keys() if k.value == 'chronicle'][0]
#            ## trawl all chronicle chapters....
#            for chapterKey,chronicleChapter in thisChar[chronColKey][chronKey].items():
#                ## and all entries...
##                print 'chronChapter'
#                for entryKey,entryValue in chronicleChapter.items():
#                    print (entryKey,entryValue)
#                    if entryKey.value is not 'year':
#                        for propertyKey,propertyValue in entryValue.items():
#                            if str(propertyValue) in unpersons:
#                                thisChar[chronColKey][chronKey][chapterKey].__delitem__(entryKey)
    titleKey = [k for k in stuff.keys() if k.value == 'title'][0]
#    intedUnpersons = set([int(unp) for unp in unpersons])
    for thisTitleKey,thisTitleValue in stuff[titleKey].items():
        ## check all titles
        for titlePropertyKey,titleProperty in thisTitleValue.items():
            ## if it has a 'previous', remove unpersons from it.
            if titlePropertyKey.value == 'previous':
                thisTitleValue[titlePropertyKey] = ListObject(4)
#            try:
#                for unperson in intedUnpersons:
#                    titleProperty.remove(unperson)
#            except ValueError:
#                pass
#            if len(titleProperty) == 0:
#                thisTitleValue.__delitem__(titlePropertyKey)
            ## if it has a 'history', remove unpersons from it.  
            ## fuck it, jsut delete the history entirely.
            elif titlePropertyKey.value == 'history':
                ## (Replace it with empty object.)
                thisTitleValue[titlePropertyKey] = GenericObject()
    return stuff

def parse(tokenDeque,depth,initialDepth):
#    print tokenList[:5]
    ## turn the tokenList into a deque:
    thisDepth = depth
    ## kill all the things
    currentObjectType = None
    lastThing = None
    currentThing = None
    currentObject = None
    expectoEqualum = None
    while len(tokenDeque) > 0:
        ## if we're just starting and don't know in what kind of object we are,
        ## just pop one:
        if currentObjectType == None:
            currentThing = tokenDeque.popleft()
            ## then check whether it's a valid thing.
            if currentThing.type in THINGTYPES:
                if lastThing == None:
                    lastThing = currentThing
                elif lastThing.type == currentThing.type:
                    currentObject = ListObject(currentThing.type)
                    currentObjectType = 'list'
                    currentObject.append(lastThing.pack())
                    currentObject.append(currentThing.pack())
                elif (lastThing.type != None) and currentThing.type in THINGTYPES:
                    currentObject = ListObject(8) ## type 8, mixed list
                    currentObjectType = 'list'
                    currentObject.append(lastThing.pack())
                    currentObject.append(currentThing.pack())                    
            elif currentThing.type == 1 and lastThing.type in THINGTYPES:
                ## last thing was a valid thing; this is an =: generic object!
                currentObject = GenericObject()
                currentObjectType = 'generic'
                ## lastThing remains stored, waiting for the next iteration of
                ## the while loop
                ## we already had the =, so we're not expecting it.
                expectoEqualum = False
                continue
            elif currentThing.value == '}' :
                ## end bracket without having gone into type? We were a list,
                ## length 0 or 1. Also, we're ending.
                currentObject = ListObject(currentThing.type)
                currentObjectType = 'list'
                if lastThing is not None:
                    ## length not 0, then. Add the 1 item.
                    currentObject.append(lastThing.pack())
                break
            elif currentThing.value == '{':
                currentObject = ListObject(7) ## type 7, list of lists.
                currentObjectType = 'list'
                value = parse(tokenDeque,thisDepth+1,initialDepth)
                currentObject.append(value)
            else:
                raise ValueError('We did not expect this thing: {0}, here: line {1}, character {2}'.format(
                        currentThing.value,currentThing.lineLocation,currentThing.characterLocation))
                break
        elif currentObjectType == 'generic':
            currentThing = tokenDeque.popleft()
            ## we're in a generic object. Add things until we hit bracket.
            if lastThing is not None:
                ## thing1 already in memory, but we haven't necessarily had an
                ## = yet. Next item should be =, a thing, or
                ## a bracket.
                if expectoEqualum and currentThing.type is not 1:
                    ## we want an =.
                    print 'Expected an equals-sign here:  line {0}, character {1}'.format(
                        currentThing.lineLocation,currentThing.characterLocation)
                elif currentThing.type == 1 and expectoEqualum:
                    ## we get an =. 
                    expectoEqualum = False
                    continue
                
                elif not expectoEqualum:
                    ## we have and =, we have something in memory. Next token 
                    ## is a thing or a {:
                    if currentThing.value == '{':
                        ## recurse!
                        payload = parse(tokenDeque,thisDepth+1,initialDepth)
                    elif currentThing.type in THINGTYPES:
                        ## it's a valid thing.
                        payload = currentThing.pack()
                    else:
                        print 'Expected curly bracket or a thing here: line {0}, character {1}'.format(
                        currentThing.lineLocation,currentThing.characterLocation)
                        break
                    ## either way, we should have gotten a value, and we 
                    ## already have a key (it's lastThing).
                    key = lastThing.pack()
                    assert not currentObject.has_key(key)
                    currentObject[key] = payload
                    ## purge the memory.
                    lastThing = None
            elif currentThing.value == '}':
                ## we're done here.
                break
            else:
                assert currentThing.type in THINGTYPES,'We did not expect this thing: {0}, here: line {1}, character {2}'.format(
                        currentThing.value,currentThing.lineLocation,currentThing.characterLocation)
                lastThing = currentThing
                expectoEqualum = True
        elif currentObjectType == 'list':
            currentThing = tokenDeque.popleft()
            ## we're in a list. Add things until we hit bracket.
            if currentThing.value == '}':
#                print 'ending the list'
                ## we're done here.
                break
            elif currentThing.type is currentObject.contentType:
                ## thing is the value we'd expect it to be.
                currentObject.append(currentThing.pack())
            elif currentObject.contentType == 7 and currentThing.value == '{':
                ## correct case for list of lists.
                value = parse(tokenDeque,thisDepth+1,initialDepth)
                currentObject.append(value)
            elif currentObject.contentType == 8:
#                print 'Wrong item type in list; unexpected case here: line {0}, character {1}'.format(
#                        currentThing.lineLocation,currentThing.characterLocation)
#                break
                ## still, if we want to implement this, it's easy...
                if currentThing.value == '{':
                    ## recurse!
                    value = parse(tokenDeque,thisDepth+1,initialDepth)
                elif currentThing.type in THINGTYPES:
                    value = currentThing.pack()
                else: 
                    print 'Unexpected list item here: line {0}, character {1}'.format(
                        currentThing.lineLocation,currentThing.characterLocation)
                    break

        else:
            print 'This should never happen.'
            break
    if depth == initialDepth:
        ## if we're ending at the bottom level, there should be no tokens
        ## left.
        assert len(tokenDeque) == 0, 'Syntax issue: too many closing brackets.'
        print [tokenDeque]
#    print repr(currentObject)
    return currentObject

class Scanner:
    def __init__(self,filePath):
        self.openFile = open(filePath,'rb')
        self.currentLineNumber = 0
        self.currentCharNumber = 0
        self.currentLine = list(self.openFile.readline())
    
    def skipLine(self):
        self.currentLine = list(self.openFile.readline())
        self.currentLineNumber += 1
        
    def getChar(self):
        c = self.currentLine.pop(0)
        self.currentCharNumber += 1
        if len(self.currentLine) == 0:
            self.currentLine = list(self.openFile.readline())
            if self.currentLine == []:
#                print 'scanner returning None and shutting down'
                self.shutDown()
                return None
#            print repr(self.currentLine)
            self.currentLineNumber += 1
        return c
        
    def getLocation(self):
        return (self.currentLineNumber,self.currentCharNumber)
        
    def shutDown(self):
        self.openFile.close()

class Tokenizer:
    def __init__(self,scanner):
        self.scanner = scanner
        self.tokenList = []
        self.currentLocation = scanner.getLocation()
        self.stopFlag = False
        currentToken = ''
        while self.stopFlag is False:
            currentToken = self.process()
#            print currentToken
            if currentToken is not None:
                self.tokenList.extend(currentToken)
                
    def process(self):
        sequenceStarted = False
        cargo = []
        tokenType = None
        extraToken = []
        while True:
            c = self.scanner.getChar()
            if c == None:
                self.stopFlag = True
                break
            elif sequenceStarted == False:
                ## scan for some sequence to start.
                if c.isspace():
                    ## if you get nothing, do nothing.
                    pass
                elif c in SYMBOLS:
                    if c == '"':
                        sequenceStarted = True
                        tokenType = 3 ## string
                        sequenceType = 'str'
                    elif c in BRACKETS:
                        tokenType = 2
                        cargo.append(c)
                        break
                    elif c == '=':
                        tokenType = 1 ## =
                        cargo.append(c)
                        break
                    elif c == '#':
                        sequenceStarted = True
                        sequenceType = 'comment'
                elif c.isalpha():
                    sequenceStarted = True
                    tokenType = 0 ## identifier
                    sequenceType = 'id'
                    cargo.append(c)
                elif c.isdigit() or c == '.' or c == '-':
                    sequenceStarted = True
                    perioded = False
                    sequenceType = 'num'
                    tokenType = 4 ## int until proven float
                    cargo.append(c)
                    
            elif sequenceStarted == True:
                if sequenceType == 'id':
                    if c.isspace():
                        break
                    elif c == '=':
                        extraToken.append(
                            self.makeToken(
                                0,
                                ''.join(cargo),
                                *self.scanner.getLocation()
                            )
                        )
                        tokenType = 1
                        cargo = c
                        break
                    else:
                        cargo.append(c)
                elif sequenceType == 'str':
                    if c == '"':
                        break
                    else:
                        cargo.append(c)
                elif sequenceType == 'num':
                    if c.isspace():
                        break
                    elif c.isdigit():
                        cargo.append(c)
                    elif c in SYMBOLS:
                        extraToken.append(
                            self.makeToken(
                                tokenType,
                                ''.join(cargo),
                                *self.scanner.getLocation()
                            )
                        )
                        tokenType = 1
                        cargo = c
                        break                        
                    elif c == '.' and perioded == True:
                        tokenType = 6 ## date
                        cargo.append(c)
                    elif c == '.':
                        perioded = True
                        tokenType = 5 ## float until proven date
                        cargo.append(c)           
                        
                elif sequenceType == 'comment':
                    if c == '\n':
                        break
            else:
                print 'Cannot into tokenize. Halp.'
        payload = ''.join(cargo)
        if tokenType == 4:
            payload = int(payload)
        elif tokenType == 5:
            payload = float(payload)
        else:
            pass             
        if tokenType is not None:              
            return extraToken+[self.makeToken(
                    tokenType,payload,*self.scanner.getLocation())]
            
    def makeToken(self,type,value,lineLocation=None,characterLocation=None):
        return Token(
            type=type,
            value=value,
            lineLocation=lineLocation,
            characterLocation=characterLocation)

class Token:
    def __init__(self,**entries):
        self.__dict__.update(entries)
    def __repr__(self):
        return str(self.value)
    def pack(self):
        if self.type == 0:
            return Identifier(self.value)
        elif self.type == 3:
            return ParadoxString(self.value)
        elif self.type == 6:
            return Date(self.value)
        else:
            return self.value

class GenericObject(collections.OrderedDict):
    def __init__(self,basePadding = '\t'):
        collections.OrderedDict.__init__(self)
        self.basePadding = basePadding
    def __repr__(self,depth = 0):
        padding = ''.join([self.basePadding*depth])
        out = ['{\n']
        for key,value in self.items():
            ## key is always monadic
            out.append(str(key)+' = ')
#            print repr(key)
            if hasattr(value, '__iter__'):
                ## a bracered thing
#                print value
                out.append(value.__repr__(depth+1)+'\n')
            elif PROVREPLACEHACK == True and key.value == 'province_id':
                if PROVMAPPING.has_key(int(value)):
                    value = PROVMAPPING[int(value)]
#                    print 'provkey flipped: {0} with {1}'.format(str(value),str(PROVMAPPING[int(value)]))
#                else:
#                    print 'mapping is no good.'
                out.append(str(value)+'\n')                
            else:
                ## a monadic thing
                out.append(str(value)+'\n')
        out += ['}']
        return ''.join(out)

class ListObject(list):
    def __init__(self,contentType,basePadding = '\t'):
        self.contentType = contentType
        self.basePadding = basePadding
    def __repr__(self,depth = 0):
        padding = ''.join([self.basePadding*depth])
        extraPadding = ['',padding][self.contentType == 7]
        out = ['{']
        for thing in self:
            if hasattr(thing, '__iter__'):
                ## a bracered thing
                out.append(extraPadding+thing.__repr__(depth+1)+' ')
            else:
                ## a monadic thing
                out.append(str(thing)+' ')
        out += ['}']
        return ''.join(out)
    
class Identifier:
    ## a thing in 'thing = thing' of paradox files, as opposed to int, float
    ## or string, for which python has native types.
    def __init__(self,value):
        self.value = value
        self.index = 0 ## there can be multiple things of the same name in a
        ## list. this is to differentiate them.
    def __repr__(self):
        return self.value
        
class Date:
    def __init__(self,value):
        assert type(value) ==  str
        assert value.count('.') == 2
        self.value = value
        self.index = 0
    def __repr__(self,depth = 0):
        return self.value

class ParadoxString:
    def __init__(self,value):
        self.value = value
    def __repr__(self):
        return '"{0}"'.format(self.value)
    
if __name__ == '__main__':
    startScan = time.time()
#    sc = Scanner('mp_Suomimaa838_02_10.ck2')
#    sc = Scanner('testfile.txt')
#    tok = Tokenizer(sc)
#    cProfile.run("deque,stuff = loadParadoxFile('mp_Suomimaa838_02_10.ck2',fileType = 'ck2save')")
    cProfile.run("deque,stuff = loadParadoxFile('testfile.txt')")
#    cProfile.run("deque,stuff = loadParadoxFile('Seville849_04_15_unc.ck2',fileType = 'ck2save')")
#    cProfile.run("deque,stuff =loadParadoxFile('faction_decisions.txt',fileType = 'ck2save')")
#    stuff = loadParadoxFile('testfile.txt',ck2save = False)
    print 'Time taken to parse file: {0}s.'.format(time.time() - startScan)
    ## prune the save
#    stuff2 = copy.deepcopy(stuff)
#    stuff2 = pruneCK2Save(stuff,70)
    ## now, reverse the process:
#    print stuff
#    dumpParadoxFile(stuff2,'outfile.ck2',fileType = 'ck2save')
    dumpParadoxFile(stuff,'outfile.txt')
    
    ## 144999 =  karloman
    ## 6392 = charlemagne
    ## 525130 = synario's char with chronicle
    

    
