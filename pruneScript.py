# -*- coding: utf-8 -*-
"""
Created on Sun Jan 18 15:51:50 2015

@author: Oddman
"""
import time,ParadoxParser,copy,cProfile

startScan = time.time()
cProfile.run("deque,stuff = ParadoxParser.loadParadoxFile('mp_Iberia1018_01_22_1.ck2',fileType = 'ck2save')")
print 'Time taken to parse file: {0}s.'.format(time.time() - startScan)
## prune the save
stuff2 = copy.deepcopy(stuff)
stuff2 = ParadoxParser.pruneCK2Save(stuff,70)
## now, reverse the process:
ParadoxParser.dumpParadoxFile(stuff,'PrunedSave_1028.ck2',fileType = 'ck2save')