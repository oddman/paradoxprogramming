# -*- coding: utf-8 -*-
"""
Created on Sat Jan 17 03:26:12 2015

@author: Oddman
"""

import time,ParadoxParser,cProfile, collections
reload(ParadoxParser)

startScan = time.time()
cProfile.run("deque,saveData = ParadoxParser.loadParadoxFile('vanilloidstart.v2')")
print 'Time taken to parse file: {0}s.'.format(time.time() - startScan)

## load the mapping
mapping = collections.OrderedDict()
with open('provinceMapping.txt','r') as provinceMapping:
    for line in provinceMapping:
        item = line.split('=')
        item = tuple([int(i.strip()) for i in item])
        mapping[item[0]] = item[1]

## for every province in the save which has a key in mapping, record its
## resource.
flippingResources = {}
randomInts = [100,101,102,108,109]
for flipInt in mapping.keys():
#for flipInt in randomInts:
    province = saveData[str(flipInt)]
    rgoKey = [key for key,value in province.items() if key.value == 'rgo'][0]
    goodsTypeKey = [key for key,value in province[rgoKey].items() if key.value == 'goods_type'][0]
    ## this is where the actual flipping happens.
#    print str(flipInt) + ' = ' + saveData[str(flipInt)][rgoKey][goodsTypeKey].value
#    print str(flipInt) + ' = ' + province[rgoKey][goodsTypeKey].value
    flippingResources[mapping[flipInt]] = province[rgoKey][goodsTypeKey].value

for provInt in mapping.keys():
    province = saveData[str(provInt)]
    rgoKey = [key for key,value in province.items() if key.value == 'rgo'][0]
    goodsTypeKey = [key for key,value in province[rgoKey].items() if key.value == 'goods_type'][0]
    print saveData[str(provInt)][rgoKey][goodsTypeKey].value + ' flipped for ' + flippingResources[provInt]
    saveData[str(provInt)][rgoKey][goodsTypeKey].value = flippingResources[provInt]

#print flippingResources

## output the result
ParadoxParser.dumpParadoxFile(saveData,'test_outfile.v2')

## load & hack the event file
deque,goodsData = ParadoxParser.loadParadoxFile('goods.txt')
ParadoxParser.PROVREPLACEHACK = True
ParadoxParser.PROVMAPPING = mapping
ParadoxParser.dumpParadoxFile(goodsData,'goods_out.txt')